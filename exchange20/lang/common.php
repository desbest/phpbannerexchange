<?
$file_rev="041305";
$file_lang="en";
// If you translate this file, *PLEASE* send it to me
// at darkrose@eschew.net

// Many of the variables contained in this file are used
// as common variables throughout the script. I have tried
// my best to include these variables in the "generic"
// section. I know many languages use different suffixes
// and what-not when used in context, so I have included
// the context in which some variables are used in the
// comments.
//
// Mail templates are located in the /templates/mail directory
// Error messages are located in the /lang/errors.php file

// Menu items..
$LANG_menu_options="Options";
$LANG_backtologin="Login";
$LANG_lostpw="Lost Password?";
$LANG_faq="FAQ";
$LANG_signup="Sign Up";
$LANG_rules="Exchange Rules";
$LANG_tocou="Terms of Service";

$LANG_menu_extras="Extras";
$LANG_topbanns="Top Banners";
$LANG_overallstats="Overall Stats";

// login page (/index.php)
$LANG_indtitle="Control Panel Login";
$LANG_headertitle="Banner Exchange Control Panel";
$LANG_login_instructions="Enter your login name and password below to access your account. If you would like to join the exchange, <a href=\"cou.php\">Click Here</a> to create a new account!";
$LANG_login="Username:";
$LANG_pw="Password:";
$LANG_login_button="Login";

//Recover Password (/recoverpw.php)
$LANG_lostpw_title="Recover password";
$LANG_lostpw_instructions="To reset your password, enter the e-mail address you used to sign up for the exchange. A new password will be generated and e-mailed to this address.";
$LANG_lostpw_instructions2=" For security reasons, it is not possible to send automatically reset passwords to a different e-mail account.";
$LANG_lostpw_recover="Your password has been reset and you will be receiving an e-mail shortly with your new password.";
$LANG_lostpw_email="E-mail Address";
$LANG_lostpw_success="Your password has been reset! You will be receiving an e-mail shortly with your new password. Once you have logged in, you may change your password from your Banner Exchange Control Panel.";

// Signup Form (/signup.php)
$LANG_signupwords="Sign up for $exchangename";
$LANG_realname="Real Name";
$LANG_pw_again="Password Again";
$LANG_cat="Category";
$LANG_catstuff="Please Select a Category";
$LANG_email="Email Address";
$LANG_siteurl="Site URL";
$LANG_bannerurl="Banner URL";
$LANG_signsub="Press Once to Submit";
$LANG_signres="Reset";
$LANG_newsletter="Send Newsletter";
$LANG_coupon="Coupon Code";
$LANG_rejected="We could not create your account for the following reasons:";

// success messages..
$LANG_signup_thanks="Thanks for signing up!";
$LANG_signupinfo="Your account has been successfully added! You will be added into the banner rotation in the next couple of days. We have also sent an email to $email with your login and password. You should keep this in a safe place for future reference.  You may also login by going to the <a href=\"index.php\">Stats Login page</a> at any time.  This panel will allow you to add banners, check your stats, change your URL, change your password, etc.<p>";
$LANG_coupon_added="Your account has been credited with <b>$newcredits</b> credits for using the coupon.";
$LANG_signup_uploadmsg="Note: You will need to log in and upload a banner in order for your account to be approved!";

// Conditions of Use page (/conditions.php)
$LANG_coutitle="Terms and Conditions";
$LANG_header="Member Agreement";
$LANG_agree="I Agree";
$LANG_disagree="I do not agree";

// Top banners/accounts page (/top.php)
//top 10 banners:
$LANG_top10_title="Top $topnum accounts";
$LANG_top10_exposure="Exposures";
$LANG_top10_banners="Banner";
$LANG_top10_nobanners="There are no approved banners in the exchange yet, so no data can be shown here! Note: Default banners are NOT shown on this page, only normal user accounts are shown!";

//Overall stats (/overall.php)
$LANG_overall_totusers="Users";
$LANG_overall_exposures="Exposures";
$LANG_overall_banners="Banners";
$LANG_overall_totclicks="Clicks to sites";
$LANG_overall_totsiteclicks="Clicks from sites";
$LANG_overall_ratio="Click-thru ratio";


// common stuff...
$LANG_yes="Yes";
$LANG_no="No";
$LANG_top="Top";
$LANG_topics="Topics";

//MAIL TEMPLATES ARE IN THE /template/mail DIRECTORY!!
?>