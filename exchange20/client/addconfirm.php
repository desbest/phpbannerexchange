<?
$file_rev="041306";
////////////////////////////////////////////////////////
//                 phpBannerExchange                  //
//                   by: Darkrose                     //
//              (darkrose@eschew.net)                 //
//                                                    //
// You can redistribute this software under the terms //
// of the GNU General Public License as published by  //
// the Free Software Foundation; either version 2 of  //
// the License, or (at your option) any later         //
// version.                                           //
//                                                    //
// You should have received a copy of the GNU General //
// Public License along with this program; if not,    //
// write to the Free Software Foundation, Inc., 59    //
// Temple Place, Suite 330, Boston, MA 02111-1307 USA //
//                                                    //
//     Copyright 2004 by eschew.net Productions.      //
//   Please keep this copyright information intact.   //
////////////////////////////////////////////////////////

include("../config.php");
include("../css.php");

if($use_gzhandler==1){
ob_start("ob_gzhandler");
}

require_once('../lib/template_class.php');
include("../lang/client.php");

// Begin login stuff
$db=mysql_connect("$dbhost","$dbuser","$dbpass");
mysql_select_db($dbname,$db);
$result = mysql_query("select * from banneruser where login='$login' AND pass='$pass'");
$get_userinfo=@mysql_fetch_array($result);
$id=$get_userinfo[id];
$login=$get_userinfo[login];
$pass=$get_userinfo[pass];

session_start();
$session=session_id();
$login=$_SESSION['login'];
$pass=$_SESSION['pass'];
$id=$_SESSION['id'];

if($login=="" AND $pass=="" OR $pass=="") {
	$page = new Page('../template/client_login_error.php');	
	$page->replace_tags(array(	
		'css' => "$css",
		'session' => "$session",	
		'baseurl' => "$baseurl",	
		'title' => "$exchangename - $LANG_login_error_title",	
		'shorttitle' => "$LANG_login_error_title",	
		'msg' => "$LANG_login_error",	
		'footer' => '../footer.php'));	
	$page->output();	
	session_destroy();
}else{

		// let's check the image...

$newbanurl=$_POST[bannerurl];
$target=$_POST[target];

if (get_magic_quotes_gpc()) {
	$newbanurl = stripslashes($newbanurl);
	$target = stripslashes($target);
}

$newbanurl=mysql_real_escape_string($newbanurl);
$target=mysql_real_escape_string($target);

$imagestuff = @getimagesize($newbanurl);
$imagewidth = $imagestuff[0];
$imageheight = $imagestuff[1];
$error=0;

//anti-css and xss.
$target = ereg_replace("~<script[^>]*>.+</script[^>]*>~isU", "", $target); 
$target = strip_tags($target);
$target = str_replace("<!--", "&lt;!--", $target);
$target = preg_replace("/(\<)(.*?)(--\>)/mi", "".nl2br("\\2")."", $target);

$newbanurl = ereg_replace("~<script[^>]*>.+</script[^>]*>~isU", "", $newbanurl); 
$newbanurl = strip_tags($newbanurl);
$newbanurl = str_replace("<!--", "&lt;!--", $newbanurl);
$bannerurl = preg_replace("/(\<)(.*?)(--\>)/mi", "".nl2br("\\2")."", $newbanurl);


			// Validate the Banner Width and Height
			if($imagewidth==''){
				$error = 1;
				$error_html .= "$LANG_addconf_err_noban<br><br>\n";
		}
			if($imagewidth != $bannerwidth){
				$error=1;
				$error_html .= "$LANG_addconf_err_width<br><br>\n";
		}
			if($imageheight != $bannerheight){
				$error=1;
				$error_html .= "$LANG_addconf_err_height<br><br>\n";
		}
		//strip 
		if($error=="1"){
					$msg.="<b>$LANG_addconf_err_explain:</b><p>";
					$msg.="$error_html";
				} else {
			mysql_query("insert into bannerurls values('','$bannerurl','$target','0','0','$id','0')");
			$msg="$LANG_addconfirm_msg";

				if ($reqbanapproval == "Y") { //Only set appoved to false if said so in config
 					mysql_query("update bannerstats set approved='0' where uid='$id'");
				}
				
				$msg= "$LANG_editbanner_message<p><img src=\"$newbanurl\">";

			$page = new Page('../template/client_addbanner_confirm.php');
			$page->replace_tags(array(
			'css' => "$css",
			'session' => "$session",
			'baseurl' => "$baseurl",
			'title' => "$exchangename - $LANG_addconfirm_title",
			'shorttitle' => "$LANG_addconfirm_title",
			'bannerurl' => "$bannerurl",
			'message' => "$msg",
			'footer' => '../footer.php',
			'menu' => 'client_menuing.php'));

			$page->output();
	}
}
?>