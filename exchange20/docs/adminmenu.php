<div id="llinks"><a NAME="top"></a>
<div class="sidetitle"><? echo "Admin Guide"; ?></div>
  <div class="side">

<a href="admin.php#introduction">Introduction</a><br>
<a href="admin.php#stats">Login/Stats</a><br>
<a href="admin.php#validate">Validating Accounts</a><br>
<a href="admin.php#addacct">Add an Account</a><br>
<a href="admin.php#listacct">List All Accounts</a><br>
<a href="admin.php#defbanner">Default Banner</a><br>
<a href="admin.php#mailer">Mailer Manager</a><br>
<a href="admin.php#cats">Categories</a><br>
<a href="admin.php#admin">Add/Remove Admin</a><br>
<a href="admin.php#pw">Change Password</a><br>
<a href="admin.php#dbtools">Database Tools</a><br>
<a href="admin.php#template">Edit Template Tools</a><br>
<a href="admin.php#variables">Variable Editor</a><br>
<a href="admin.php#cssedit">Edit Style Sheet</a><br>
<a href="admin.php#faq">FAQ Manager</a><br>
<a href="admin.php#bannercheck">Banner Check</a><br>
<a href="admin.php#cou">Edit COU/Rules</a><br>
<a href="admin.php#promo">Promo Manager</a><br>
<a href="admin.php#update">Update Manager</a><br>
<a href="admin.php#pause">Pause Exchange</a><br>
<a href="admin.php#commerce">Selling Credits</a><br>
<a href="admin.php#nav">Navigation</a><br>
<a href="admin.php#gethelp">Getting Help/Bugs</a>
  </div>
  </div>
  </div>
