<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>{title}</title>
<link rel="stylesheet" href="{baseurl}/template/css/{css}" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" 
  marginheight="0" >
<div id="content">
<div class="main">
<table border="0" cellpadding="1" width="650" cellspacing="0">
<tr>
<td>
<table cellpadding="5" border="1" width="100%" cellspacing="0">
<tr>
<td colspan="2" class="tablehead"><center><div class="head">{title}</center></div></td>
</tr>
<td class="tablebody" colspan="2">
<div class="mainbody">
<table border="0" cellpadding="1" cellspacing="1" style="border-collapse: collapse"  width="90%">
  <tr>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="90%">
<tr>
<p>
<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="100%"><tr><td colspan="2"><center><img src="{bannerurl}"><br><a href="{url}">{url}</a><p></td></tr>
<tr><td><table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="95%">
<tr class="tablehead"><td colspan="3" class="tablehead"><center><b>{from}</b></center></td></tr>
<tr class="tablehead"><td class="tablehead"><center><b>{date}</b></center></td><td class="tablehead"><center><b>{ip}</b></center></td></tr>
{data1}
</td></tr></table>
</td>
<td valign="top"><table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse" width="95%" align="left">
<tr class="tablehead"><td colspan="3" class="tablehead"><center><b>{to}</b></center></td></tr>
<tr class="tablehead"><td class="tablehead"><center><b>{date}</b></center></td><td class="tablehead"><center><b>{ip}</b></center></td></tr>
{data2}
</td></tr></table>
</td><tr></table>
<p>
{back}
</td><tr></table>
</center>
</td>
</tr>
<tr>
<td align="right" colspan="2">
</div>
</td>
</tr>
</table>
</td>
</tr>
</table><p>
</div>
</div>
<div class="footer">
{footer}
</div>
</div>
{menu}