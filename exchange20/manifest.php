<?php

//
// Leave all of this alone. This file is populated
// via the Update manager!
//

// General Config
$timestamp = "1115134941";
$updateurl = "http://www.eschew.net/scripts/phpbe/2.0/updates/master.xml";

// Common Section
$FILE_common_cou = "041305";
$FILE_common_click = "041305";
$FILE_common_menu = "041305";
$FILE_common_cookies = "041305";
$FILE_common_dblog = "041305";
$FILE_common_faq = "041305";
$FILE_common_footer = "041305";
$FILE_common_index = "041305";
$FILE_common_overall = "041305";
$FILE_common_recoverpw = "041305";
$FILE_common_resetpw = "041305";
$FILE_common_rules = "041305";
$FILE_common_signup = "041305";
$FILE_common_signconf = "041305";
$FILE_common_top = "041305";
$FILE_common_view = "041305";

// Common Section
$FILE_user_addconfirm = "041305";
$FILE_user_banners = "041305";
$FILE_user_category = "041305";
$FILE_user_categoryconf = "041305";
$FILE_user_changeurlconf = "041305";
$FILE_user_clicklog = "041305";
$FILE_user_menu = "041305";
$FILE_user_commerce = "041305";
$FILE_user_delban = "041305";
$FILE_user_delbanconf = "041305";
$FILE_user_editbanner = "041305";
$FILE_user_editinfo = "041305";
$FILE_user_editpass = "041305";
$FILE_user_emailstats = "041305";
$FILE_user_gethtml = "041305";
$FILE_user_index = "041305";
$FILE_user_logout = "041305";
$FILE_user_infoconfirm = "041305";
$FILE_user_passconfirm = "041305";
$FILE_user_promo = "041305";
$FILE_user_remove = "041305";
$FILE_user_stats = "041305";
$FILE_user_uploadbanner = "041305";

// Admin Section
$FILE_admin_addacct = "041305";
$FILE_admin_addacctconf = "041305";
$FILE_admin_addadmin = "041305";
$FILE_admin_addcat = "041305";
$FILE_admin_menu = "041305";
$FILE_admin_adminconf = "041305";
$FILE_admin_banners = "041305";
$FILE_admin_catmain = "041305";
$FILE_admin_changedefban = "041305";
$FILE_admin_checkbanners = "041305";
$FILE_admin_checkbannersgo = "041305";
$FILE_admin_commerce = "041305";
$FILE_admin_commercedisp = "041305";
$FILE_admin_commercedit = "041305";
$FILE_admin_dbdump = "041305";
$FILE_admin_dbrestore = "041305";
$FILE_admin_dbtools = "041305";
$FILE_admin_dbupload = "041305";
$FILE_admin_deladmin = "041305";
$FILE_admin_deladminconf = "041305";
$FILE_admin_delcat = "041305";
$FILE_admin_delcatconf = "041305";
$FILE_admin_delacct = "041305";
$FILE_admin_delacctconf = "041305";
$FILE_admin_delbanner = "041305";
$FILE_admin_doupdate = "041305";
$FILE_admin_edit = "041305";
$FILE_admin_editcat = "041305";
$FILE_admin_editcatconfirm = "041305";
$FILE_admin_editconf = "041305";
$FILE_admin_editcss = "041305";
$FILE_admin_editpass = "041305";
$FILE_admin_editstuff = "041305";
$FILE_admin_editvars = "041305";
$FILE_admin_email = "041305";
$FILE_admin_emailgo = "041305";
$FILE_admin_emailsend = "041305";
$FILE_admin_emailuser = "041305";
$FILE_admin_emailusergo = "041305";
$FILE_admin_faq = "041305";
$FILE_admin_faqdel = "041305";
$FILE_admin_faqedit = "041305";
$FILE_admin_index = "041305";
$FILE_admin_listall = "041305";
$FILE_admin_logout = "041305";
$FILE_admin_pause = "041305";
$FILE_admin_processedit = "041305";
$FILE_admin_processfaq = "041305";
$FILE_admin_processvars = "041305";
$FILE_admin_promodetails = "041305";
$FILE_admin_promos = "041305";
$FILE_admin_pwconfirm = "041305";
$FILE_admin_rmbackup = "041305";
$FILE_admin_stats = "041305";
$FILE_admin_templates = "041305";
$FILE_admin_templateedit = "041305";
$FILE_admin_update = "041305";
$FILE_admin_uploadbanner = "041305";
$FILE_admin_validate = "041305";

// Libraries
$FILE_lib_manifest_upd_class = "041305";
$FILE_lib_ipn_in = "041305";
$FILE_lib_template = "041305";
$FILE_lib_ipnlib = "041305";
$FILE_lib_paypalconf = "041305";
$FILE_lib_class_compare = "041305";

// Language Files
$FILE_lang_type = "EN";
$FILE_lang_admin = "041305";
$FILE_lang_client = "041305";
$FILE_lang_common = "041305";
$FILE_lang_errors = "041305";
?>