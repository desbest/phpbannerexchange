phpBannerExchange v2

phpBannerExchange is a PHP/mySQL script that allows virtually anyone with minimal knowledge of PHP, mySQL and web hosting to run their own banner exchange. This script was inspired by some of the greatest ad rotation scripts on the Internet such as Webadverts and phpAdsNew. While these applications are great in their own right, they are primarily banner rotation scripts and not really designed to work as an exchange script. phpBannerExchange was designed by Banner Exchange administrators for Banner Exchange Administrators.

This script was made in the php4 days and it is not compatible with php7. I do not have the time to upgrade it to php7 as I have a LOT of personal projects to do.

https://desbest.com/projects/phpbannerexchange/